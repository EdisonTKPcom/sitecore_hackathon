const express = require('express')
const bodyParser = require('body-parser')
const PORT_NUMBER = process.env.PORT || 8003
const server = express()

const https = require('https')
const http = require('http')
const aws = require('aws-sdk')
const fetch = require('node-fetch');
const rek = new aws.Rekognition({ region: 'us-west-2' })
process.env.AWS_ACCESS_KEY_ID = '****'
process.env.AWS_SECRET_ACCESS_KEY = '****'
const cleanIndexAPI = '****'

function rawBodySaver(req, res, buf, encoding) {
  if (buf && buf.length) {
    req.rawBody = buf
  }
}

function getBytesData(imageUrl) {
  return new Promise((resolve, reject) => {
    let proto
    switch (imageUrl.slice(0, 5)) {
      case 'https':
        proto = https
        break
      case 'http:':
        proto = http
        break
      default:
        reject(new Error('Invalid URL'))
    }
    proto.get(imageUrl, (res) => {
      const data = []
      res.on('data', (chunk) => { data.push(chunk) })
      res.on('end', () => {
        resolve(Buffer.concat(data))
      })
    }).on('error', (e) => {
      reject(`Got error: ${e.message}`)
    })
  })
}

function detectLabels(imageUrl) {
  return new Promise((resolve, reject) => {
    const MaxLabels = 10
    const MinConfidence = 70
    Promise.resolve()
      .then(() => getBytesData(imageUrl))
      .then(async (bytesData) => {
        let Image = { Bytes: bytesData }
        const params = {
          Image,
          MaxLabels,
          MinConfidence,
        }
        rek.detectLabels(params, (err, data) => {
          if (err) {
            console.log(err, err.stack)
            reject(err)
          } else {
            resolve(data)
          }
        })
      })
      .catch(err => reject(err))
  })
}

// TODO: should limit this to ONE endpoint
server.use(bodyParser.json({ verify: rawBodySaver, limit: '10mb' }))
server.use(bodyParser.urlencoded({ extended: true }))

server.get('/', (req, res) => res.send(200))

server.get('/image', (req, res) => {
  let imageUrl = req.body.imageUrl
  if (req.body.apiKey != '*****') {
    return res.status(400).json({ error: 'invalid api key' })
  } else {
    detectLabels(imageUrl)
      .then((data) => {
        const cleanIndexData = {
          imageUrl: imageUrl,
          apiKey: cleanIndexAPI
        }
        fetch('https://image-clean-index.herokuapp.com/', {
          mode: 'cors',
          method: 'GET',
          body: JSON.stringify(cleanIndexData),
          headers: {'Content-Type': 'application/json'},
        })
          .then(res => res.json())
          .then(json => {
            return res.status(200).json({
              label: data,
              cleanIndex: json,
            })
          });
      })
  }
})
server.listen(PORT_NUMBER, (err) => {
  if (err) console.error(err)
})

